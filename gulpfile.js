var gulp = require('gulp'),
    gutil = require('gulp-util'),
    jade = require('gulp-jade'),
    livereload = require('gulp-livereload'),
    webserver = require('gulp-webserver'),
    sass = require('gulp-sass'),
    jshint = require('gulp-jshint'),
    uglify=require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps');

// define the default task and add the watch task to it
gulp.task('default', ['watch', 'webserver']);

gulp.task('jade', function() {
    return gulp.src('src/templates/*.jade')
        .pipe(jade())
        .on('error', onError) // pip to jade plugin
        .pipe(gulp.dest('build'))
        .pipe(livereload()); // tell gulp our output folder
});

gulp.task('build-css', function() {
    return gulp.src('src/sass/*.sass')
        .pipe(sass())
        .on('error', onError)
        .pipe(gulp.dest('build/css'))
        .pipe(livereload());
});

gulp.task('jshint', function() {
  return gulp.src('source/javascript/**/*.js')
    .pipe(jshint())
    .on('error', onError)
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('build-js', function() {
    return gulp.src('src/javascript/**/*.js')
        .pipe(uglify())
        .on('error', onError)
        .pipe(gulp.dest('build/javascript'))
        .pipe(livereload());
});


gulp.task('webserver', function() {
    gulp.src('./build')
        .pipe(webserver({
            livereload: true,
            directoryListing: {
                enable: true,
                path: './build'
            },
            open: true
        }));
});

function onError(err) {
  console.log(err);
  this.emit('end');
}

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('src/javascript/**/*.js', ['jshint','build-js']);
    gulp.watch('src/templates/**/*.jade', ['jade']);
    gulp.watch('src/sass/**/*.sass', ['build-css']);
});
